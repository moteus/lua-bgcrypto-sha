lua-bgcrypto-sha
================
[![Build Status](https://travis-ci.org/moteus/lua-bgcrypto-sha.png?branch=master)](https://travis-ci.org/moteus/lua-bgcrypto-sha)

## Usage
See [AesFileEncrypt](examples/AesFileEncrypt.lua) implementation using `lua-bgcrypto` library.

[![Bitdeli Badge](https://d2weczhvl823v0.cloudfront.net/moteus/lua-bgcrypto-sha/trend.png)](https://bitdeli.com/free "Bitdeli Badge")

